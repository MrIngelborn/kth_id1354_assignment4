
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

// const files = require.context('./', true, /\.vue$/i)

// files.keys().map(key => {
//     return Vue.component(_.last(key.split('/')).split('.')[0], files(key))
// })

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/*
const app = new Vue({
    el: '#app'
});
*/

function set_token(token) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': token
        }
    });
}

function set_user(user) {
    window.current_user = user;
}

$( document ).ajaxError(function( event, jqxhr, settings, thrownError ) {
    if ( jqxhr.status == 419 ) {
        location.reload();
    }
});

$('document').ready(function() {
    set_token($('meta[name="csrf-token"]').attr('content'));
    set_user($('meta[name="current_user"]').attr('content'));
    
    $('#logout-form').submit(function(event) {
        event.preventDefault();
        form = this;
        
        $.ajax({
            url:'/logout',
            type:'POST',
            data: $(this).serializeArray(),
            success: function(data) {
                // Session invalid, must reload
                location.reload();
            },
            error: function (data) {
                $(form).off("submit");
                form.submit();
            }
        });
    });
    
    $('#login-form').submit(function(event) {
        event.preventDefault();
        form = this;
        
        $.ajax({
            url:'/login',
            type:'POST',
            data: $(this).serializeArray(),
            success: function(data) {
                // Session invalid, must reload
                location.reload();
            },
            error: function (data) {
                $(form).find('#email').addClass('invalid');
                $(form).find('#password').addClass('invalid');
                return;
                $(form).off("submit");
                form.submit();
            }
        });
    });
    
    console.log('Registered jquery functions');
    
    
});