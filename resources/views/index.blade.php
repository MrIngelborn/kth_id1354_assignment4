@extends('layouts.app')

@section('content')

    <h1>Tasty Recipes</h1>
    <p>
    	Welcome to Tasty Recipes! Here we provide only to best tasting recipes in the world. Hope you find something you like!
    </p>
    <h2>One new recipe every day!</h2>
    <p>
    	Wondering what to eat tonight? Need some inspiration? <br/>
    	Don't worry. Take a look at our 
    	<a href="calendar.html">calendar</a> 
    	where we publish a completely new recipe every day. 
    	There you can can also see the all the previously published recipes.
    </p>

@endsection