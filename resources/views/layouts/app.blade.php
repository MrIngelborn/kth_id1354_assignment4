<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width,initial-scale=1.0" />
		
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="current_user" content=@auth "{{ Auth::user()->id }}" @else "-1" @endauth>
		
        <!-- Styles -->
		@section('css')
    		<link type="text/css" rel="stylesheet" href="{{ asset('css/main.css') }}">
    		<link type="text/css" rel="stylesheet" media="screen and (min-width:600px)" href="{{ asset('css/main_600.css') }}">
    		<link type="text/css" rel="stylesheet" href="{{ asset('css/header.css') }}">
    		<link type="text/css" rel="stylesheet" media="screen and (min-width:600px)" href="{{ asset('css/header_600.css') }}">
		@show
		
        <!-- Javascript -->
		@section('js')
            <script type="text/javascript" lang="javascript" src="{{ asset('js/app.js') }}"></script>
		@show
		
		<title>
		    Tasty Recipes
		    @hasSection('title')
    		    - @yield('title')
		    @endisset
        </title>
	</head>
	<body>
    	<header>
        	<a href="{{ route('home') }}" id="title">
                <h1>{{ config('app.name', 'Tasty Recipes') }}</h1>
        	</a>
        	@section('login')
                @include('includes.login')
        	@show
    		@include('includes.mainnav')
    	</header>
		<section class="@yield('content_class')">
			@yield('content')
		</section>
	</body>
</html>