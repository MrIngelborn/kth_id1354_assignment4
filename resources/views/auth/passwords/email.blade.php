@extends('layouts.app')

@section('css')
    @parent
	<link type="text/css" rel="stylesheet" href="{{ asset('css/form.css') }}">
	<link type="text/css" rel="stylesheet" media="screen and (min-width:600px)" href="{{ asset('css/form_600.css') }}">
@endsection

@section('title', 'Reset Password')

@section('content_class', 'form')

@section('content')
    <h2>{{ __('Reset Password') }}</h2>
    
    @if (session('status'))
        <p>
            {{ session('status') }}
        </p>
    @endif
    
    <form method="POST" action="{{ route('password.email') }}">
        @csrf
    
        <label for="email">{{ __('E-Mail Address') }}</label>
        <input 
                id="email" 
                type="email" 
                class="{{ $errors->has('email') ? 'invalid' : '' }}"
                name="email" 
                value="{{ old('email') }}" 
                required="required"
        />
    
        @if ($errors->has('email'))
            <span class="error">{{ $errors->first('email') }}</span>
        @endif
        
        <input type="submit" value="{{ __('Send Password Reset Link') }}" />
    </form>
@endsection
