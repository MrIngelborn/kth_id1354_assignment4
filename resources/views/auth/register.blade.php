@extends('layouts.app')

@section('css')
    @parent
	<link type="text/css" rel="stylesheet" href="{{ asset('css/form.css') }}">
	<link type="text/css" rel="stylesheet" media="screen and (min-width:600px)" href="{{ asset('css/form_600.css') }}">
@endsection

@section('title', 'Register')

@section('content_class', 'form')

@section('content')
    <h1>{{ __('Register') }}</h1>
    
    <form method="POST" action="{{ route('register') }}">
    @csrf
    
        <label for="name">{{ __('Name') }}</label>
        <input 
                id="name" 
                type="text" 
                class="{{ $errors->has('name') ? 'invalid' : '' }}"
                name="name" 
                value="{{ old('name') }}"
                required="required"
                autofocus="autofocus"
        />
        @if ($errors->has('name'))
            <span class="error">{{ $errors->first('name') }}</span>
        @endif
        
        <label for="email">{{ __('E-Mail Address') }}</label>
        <input 
                id="email" 
                type="email" 
                class="{{ $errors->has('email') ? 'is-invalid' : '' }}" 
                name="email" 
                value="{{ old('email') }}" 
                required="required"
        />

        @if ($errors->has('email'))
            <span class="error">{{ $errors->first('email') }}</span>
        @endif
        
        <label for="password">{{ __('Password') }}</label>
        <input 
                id="password" 
                type="password" 
                class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" 
                name="password" 
                required="required"
        />

        @if ($errors->has('password'))
            <span class="error">{{ $errors->first('password') }}</span>
        @endif
        
        <label for="password-confirm">{{ __('Confirm Password') }}</label>
        <input id="password-confirm" type="password" name="password_confirmation" required="required">
        
        <input type="submit" value="{{ __('Register') }}" />
    </form>
@endsection
