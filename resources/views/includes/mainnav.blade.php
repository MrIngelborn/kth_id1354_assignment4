<nav>
	<a href="/" class="{{ Request::is('/') ? 'active' : '' }}">
		Home
	</a>
	<a href="/calendar" class="{{ Request::is('calendar') ? 'active' : '' }}">
		Calendar
	</a>
	<a href="/recipe/meatballs" class="{{ Request::is('recipe/meatballs') ? 'active' : '' }}">
		Meatballs
	</a>
	<a href="/recipe/pancakes" class="{{ Request::is('recipe/pancakes') ? 'active' : '' }}">
		Pancakes
	</a>
</nav>