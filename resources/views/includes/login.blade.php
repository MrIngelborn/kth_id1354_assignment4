<!-- Authentication Form -->
<hgroup id="login" @auth style="display: none" @endauth>
    <form id="login-form" method="POST" action="{{ route('login') }}" >
        @csrf
        
        <input id="email" type="email" name="email" required="required" placeholder="{{ __('E-mail address') }}" class="{{ $errors->has('email') ? ' invalid' : '' }}" value="{{ old('email') }}" />
        
        <input id="password" type="password" name="password" required="required" placeholder="{{ __('Password') }}" class="{{ $errors->has('email') ? ' invalid' : '' }}" />
        
        <input type="submit" value="{{ __('Login') }}" />
        
        <a href="{{ route('register') }}">
            <button type="button">{{ __('Register') }}</button>
        </a>
        
        <a href="{{ route('password.request') }}">
            <button type="button">{{ __('Forgot Password') }}</button>
        </a>
    </form>
</hgroup>
<hgroup id="logout" @guest style="display: none" @endguest>
    <h2>@auth{{ Auth::user()->name }}@endauth</h2>
    <form id="logout-form" action="{{ route('logout') }}" method="POST">
        @csrf
        <input type="submit" value="{{ __('Logout') }}" />
    </form>
</hgroup>
