@extends('layouts.app')

@section('css')
    @parent
	<link type="text/css" rel="stylesheet" href="{{ asset('css/calendar.css') }}" />
	<link type="text/css" rel="stylesheet" media="screen and (min-width:600px)" href="{{ asset('css/calendar_600.css') }}">
@endsection

@section('title', 'Calendar')

@section('content_class', 'calendar')
    		
@section('content')

<h2>November 2018</h2>
<div class="calendar">
	<span class="day_of_week">Monday</span>
	<span class="day_of_week">Tuesday</span>
	<span class="day_of_week">Wednesday</span>
	<span class="day_of_week">Thursday</span>
	<span class="day_of_week">Friday</span>
	<span class="day_of_week">Saturday</span>
	<span class="day_of_week">Sunday</span>
	
	<span class="week">44</span>
	<span class="week">45</span>
	<span class="week">46</span>
	<span class="week">47</span>
	<span class="week">48</span>
	
	<span class="calendar-item other_month">
		<span class="date">30</span>
	</span>
	<span class="calendar-item other_month">
		<span class="date">30</span>
	</span>
	<span class="calendar-item other_month">
		<span class="date">31</span>
	</span>
	<span class="calendar-item">
		<span class="date">1</span>
	</span>
	<span class="calendar-item">
		<span class="date">2</span>
	</span>
	<span class="calendar-item">
		<span class="date">3</span>
	</span>
	<span class="calendar-item">
		<span class="date">4</span>
	</span>
	<span class="calendar-item">
		<span class="date">5</span>
	</span>
	<span class="calendar-item">
		<span class="date">6</span>
		<span class="recipe_img">
			<a href="recipe_pancakes.html">
				<img alt="Image of pancakes" src="img/recipes/pancakes.jpg" />
			</a>
		</span>
	</span>
	<span class="calendar-item">
		<span class="date today">7</span>
		<span class="recipe_img">
			<a href="recipe_meatballs.html">
				<img alt="Image of meatballs" src="img/recipes/meatballs.jpg" />
			</a>
		</span>
	</span>
	<span class="calendar-item">
		<span class="date">8</span>
	</span>
	<span class="calendar-item">
		<span class="date">9</span>
	</span>
	<span class="calendar-item">
		<span class="date">10</span>
	</span>
	<span class="calendar-item">
		<span class="date">11</span>
	</span>
	<span class="calendar-item">
		<span class="date">12</span>
	</span>
	<span class="calendar-item">
		<span class="date">13</span>
	</span>
	<span class="calendar-item">
		<span class="date">14</span>
	</span>
	<span class="calendar-item">
		<span class="date">15</span>
	</span>
	<span class="calendar-item">
		<span class="date">16</span>
	</span>
	<span class="calendar-item">
		<span class="date">17</span>
	</span>
	<span class="calendar-item">
		<span class="date">18</span>
	</span>
	<span class="calendar-item">
		<span class="date">19</span>
	</span>
	<span class="calendar-item">
		<span class="date">20</span>
	</span>
	<span class="calendar-item">
		<span class="date">21</span>
	</span>
	<span class="calendar-item">
		<span class="date">22</span>
	</span>
	<span class="calendar-item">
		<span class="date">23</span>
	</span>
	<span class="calendar-item">
		<span class="date">24</span>
	</span>
	<span class="calendar-item">
		<span class="date">25</span>
	</span>
	<span class="calendar-item">
		<span class="date">26</span>
	</span>
	<span class="calendar-item">
		<span class="date">27</span>
	</span>
	<span class="calendar-item">
		<span class="date">28</span>
	</span>
	<span class="calendar-item">
		<span class="date">29</span>
	</span>
	<span class="calendar-item">
		<span class="date">30</span>
	</span>
	<span class="calendar-item other_month">
		<span class="date">1</span>
	</span>
	<span class="calendar-item other_month">
		<span class="date">2</span>
	</span>
</div>
@endsection