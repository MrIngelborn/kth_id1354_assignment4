@extends('layouts.app')

@section('css')
    @parent
	<link type="text/css" rel="stylesheet" href="{{ asset('css/recipe.css') }}">
	<link type="text/css" rel="stylesheet" media="screen and (min-width:600px)" href="{{ asset('css/recipe_600.css') }}">
	<link type="text/css" rel="stylesheet" href="{{ asset('css/form.css') }}">
	<link type="text/css" rel="stylesheet" media="screen and (min-width:600px)" href="{{ asset('css/form_600.css') }}">
@endsection

@section('js')
    @parent
    <script type="text/javascript" lang="javascript">
        function add_comments(comments) {
            var template = $('#comment_template');
            comments.forEach(function(item, index){
                var clone = template.clone();
                clone.show();
                clone.removeAttr('id');
                clone.find('.name').text(item.username);
                clone.find('.date').text(item.created_at);
                clone.find('p').text(item.comment).html(clone.find('p').html().replace(/\n/, '<br/>'));
                if (window.current_user == item.user_id) {
                    clone.find('form').attr('action', clone.find('form').attr('action') + '/' + item.id);
                    clone.find('form').submit(function(event) {
                        event.preventDefault();
                        form = this;
                        
                        $.ajax({
                            url: $(form).attr('action'),
                            type: $(form).attr('method'),
                            data: $(form).serializeArray(),
                            success: function(data) {
                                $(form).parent().remove();
                            },
                            error: function (data) {
                                console.error(data);
                            }
                        });
                    });
                }
                else {
                    clone.find('form').remove();
                }
                template.after(clone);
            });
        }
        
        $(document).ready(function() {
            $.ajax({
                url:'{{ route('comments.get', $recipe->url) }}',
                type: 'GET',
                success: function(data) {
                    console.log(data);
                    add_comments(data);
                },
                error: function (data) {
                    console.log(data);
                }
            });
            
            $("#new_comment_form").submit(function(event) {
                event.preventDefault();
                form = this;
                
                $.ajax({
                    url: $(form).attr('action'),
                    type: $(form).attr('method'),
                    data: $(form).serializeArray(),
                    success: function(data) {
                        console.log(data);
                        add_comments(data);
                    },
                    error: function (data) {
                        console.error(data);
                    }
                });
            });
        });
    </script>
@endsection

@section('title', $recipe->title)

@section('content_class', 'recipe')
    		
@section('content')
    
    <div id="title">
		<h1>{{ $recipe->title }}</h1>
	</div>
	<div id="image">
		<img  alt="Imgage of {{ $recipe->url }}" src="{{ $recipe->imageurl }}"/>
	</div>
	<div id="summary">
		<div class="notes">
			<div class="note">
				<span class="title">Prep time</span>
				<span class="info">{{ $recipe->preptime }}</span>
			</div>
			<div class="note">
				<span class="title">Cook time</span>
				<span class="info">{{ $recipe->cooktime }}</span>
			</div>
			<div class="note">
				<span class="title">Servings</span>
				<span class="info">{{ $recipe->quantity }}</span>
			</div>
		</div>
		<p>
    		{{ $recipe->description }}
		</p>
	</div>
	<div id="ingredients">
		<h2>Ingredients</h2>
		<ul>
    		@foreach ($recipe->ingredients as $ingredient)
    		    <li>{{ $ingredient->ingredient }}</li>
    		@endforeach
		</ul>
	</div>
	<div id="instructions">
		<h2>Instructions</h2>
		<ol>
    		@foreach ($recipe->instructions as $instruction)
    		    <li>{{ $instruction->instruction }}</li>
    		@endforeach
		</ol>
	</div>
	<div id="comments">
		<h2>Comments</h2>
		@auth
		<form id="new_comment_form" action="{{ route('comment.create', $name) }}" method="post">
    		@csrf
			<label for="comment">Comment:</label>
            @if ($errors->has('comment'))
                <span class="error">{{ $errors->first('comment') }}</span>
            @endif
			<textarea id="comment" name="comment" placeholder="Your message">{{ old('comment') }}</textarea>
			<input type="submit" value="Send" />
		</form>
		@endauth
		<div id="comment_template" class="comment" style="display: none">
    		<span class="name"></span>
    		<span class="date"></span>
    		<p></p>
    		<form action="{{ route('comment.destroy', [$recipe->url, null]) }}" method="post">
                @csrf
			    @method('DELETE')
                <input type="submit" value="Delete comment"/>
		    </form>
		</div>
	</div>
    
@endsection