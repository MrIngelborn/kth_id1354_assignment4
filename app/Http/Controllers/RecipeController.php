<?php
    
namespace App\Http\Controllers;

use App\RecipeComment;
use App\Recipe;

class RecipeController extends Controller
{
    /**
    *   Display a recipe
    *   
    *   @param $name Name (id) of the recipe
    */
    public function show(string $name)
    {
        $recipe = Recipe::where('url', $name)->firstOrFail();
        
        return view('recipe')
                ->with('recipe', $recipe)
                ->with('name', $name)
                ->with('comments', $recipe->comments);
        
    }
}