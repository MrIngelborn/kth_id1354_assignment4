<?php
    
namespace App\Http\Controllers;

class IndexController extends Controller
{
    
    public function show()
    {
        return response()->view('index')->header('Cache-Control', 'must-revalidate, max-age: 3600');
    }
}