<?php
namespace App\Http\Controllers;

use App\RecipeComment;
use App\Recipe;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class RecipeCommentController extends Controller
{
    public function create(Request $request, string $name)
    {
        $request->validate([
            'comment' => 'required|min:5'
        ]);
        
        if (Auth::check()) {
            $comment = new RecipeComment;
            $comment->user_id = Auth::id();
            $comment->recipe = $name;
            $comment->comment = $request->comment;
            $comment->save();
        }
        
        if ($request->ajax()) {
            $collection = collect([$comment])->map(function ($comment) {
                $username = User::find($comment->user_id)->name;
                $comment->username = $username;
                return $comment;
            });
            return $collection->values();
        }
        
        return redirect(URL::previous());
    }
    
    public function destroy(Request $request, string $name, int $comment_id)
    {
        RecipeComment::where('id', $comment_id)->where('user_id', Auth::id())->delete();
        
        if ($request->ajax()) {
            return 'success';
        }
        
        return redirect(URL::previous());
    }
    
    public function getByRecipe(Request $request, string $name) {
        $comments = Recipe::where('url', $name)->firstOrFail()->comments;
        $collection = collect($comments)->map(function ($comment) {
            $username = User::find($comment->user_id)->name;
            $comment->username = $username;
            return $comment;
        });
        
        return response()->json($collection->values());
    }
}