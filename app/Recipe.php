<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $table = 'recipes';
    
    public function comments()
    {
        return $this->hasMany('\App\RecipeComment', 'recipe', 'url');
    }
    
    public function ingredients()
    {
        return $this->hasMany('App\RecipeIngredient');
    }
    
    public function instructions()
    {
        return $this->hasMany('App\RecipeInstruction');
    }
}
